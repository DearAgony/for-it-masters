import { Map } from "immutable";

const reducer = function (state = Map(), action) {
    switch (action.type) {
        case "SET_STATE":
            let a = state.merge(action.state);
            return a;
        case "ADD_ITEM":
            return state.update("items", function (items) {
                items.push(action.item);
                return [...items];
            });
        case "DELETE_ITEM":
            return state.update("items",function(items){
                 items.splice(items.findIndex(item => item.Id == action.itemId), 1)
                 return [...items];
                }
            );
        case "SAVE_ITEM_CHANGES":
            return state.update("items",
                (items) => {
                    const index = items.findIndex(item => item.Id == action.itemId);
                    items[index].Name = action.itemName;
                    return [...items];
                }
            );
        case "TO_DONE_ITEM":
            return state.update("items",
                (items) => {
                    const index = items.findIndex(item => item.Id == action.itemId);
                    items[index].IsDone = true;
                    return [...items];
                }
            );
    }
    return state;
}
export default reducer;