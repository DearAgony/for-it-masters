var addItem = function (item) {
    return {
        type: "ADD_ITEM",
        item
    };
};
var deleteItem = function (itemId) {
    return {
        type: "DELETE_ITEM",
        itemId
    };
};
var saveItemChanges = function (itemId, itemName){
    return {
        type: "SAVE_ITEM_CHANGES",
        itemId, 
        itemName
    };
};
var toDoneItem = function (itemId) {
    return {
        type: "TO_DONE_ITEM",
        itemId,
    };
};

export default {
    addItem,
    deleteItem,
    saveItemChanges,
    toDoneItem
};