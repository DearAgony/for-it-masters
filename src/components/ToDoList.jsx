import React from "react";

class ToDoList extends React.PureComponent{
    render(){
        return <ul className="todo">
            {
                this.props.children     
            }
        </ul>
    }
}

export{
    ToDoList
}