
import React, { Fragment } from "react";
import { Button } from "./Button";
import { ToDoList } from "./ToDoList"
import { ToDoListItem } from "./ToDoListItem";
import {connect} from "react-redux";
import Actions from "../Redux/Actions";



class ToDo extends React.Component {
    nextId = 0;
    get NextId() {
        return this.nextId++;
    }
    constructor(props) {
        super(props);
        this.state = {
            inputNewTaskValue: "",
        }
    }

    onDelete = (id) => {
        this.props.deleteItem(id);
    }
    setNewTask = (e) => {
        e.preventDefault();
        if (this.state.inputNewTaskValue == "") {
            return
        }
        let newTask = {
            Name: this.state.inputNewTaskValue,
            IsDone: false,
            Id: this.NextId,
        };
        this.props.addItem(newTask);
        this.setState({inputNewTaskValue: ""});
    }
    inputChange = (event) => {
        this.setState({ inputNewTaskValue: event.target.value });
    }
    render() {
        let activeItems = this.props.items.filter(item => !item.IsDone);
        let doneItems = this.props.items.filter(item => item.IsDone);
        return <div>
            <form onSubmit={this.setNewTask}>
                <input className="taskForm" minLength="2" maxLength="30" type="text" value={this.state.inputNewTaskValue} onChange={this.inputChange} placeholder="Введите задачу" /></form><br />
            {
                <Fragment>
                    <ToDoList>
                        {
                            activeItems.map(item => <ToDoListItem key={item.Id}
                                onSave={this.props.saveItemChanges}
                                toDone={this.props.toDoneItem}
                                className="listitem"
                                item={item}
                                onDelete={() => this.onDelete(item.Id)}
                                type="checkbox" />)

                        }
                        {
                            doneItems.map(item => <ToDoListItem
                                onSave={this.props.saveItemChanges}
                                toDone={this.props.toDoneItem}
                                className="hiddenBox"
                                key={item.Id} 
                                item={item}
                                onDelete={() => this.onDelete(item.Id)}
                                type="hidden" />
                            )
                        }

                    </ToDoList>
                </Fragment>
            }
        </div>
    }
}

function mapStateToProps(state) {
    let b = { items: state.get("items")};
    return {
        items: state.get("items")
    };
}

export default connect(mapStateToProps, Actions)(ToDo);
