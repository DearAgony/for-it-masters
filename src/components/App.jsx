import React, { Fragment } from "react";
import ToDo from "./ToDo";
import "../styles/styles.css";
import { Provider } from "react-redux";
import {createStore} from "redux";
import ToDoReducer from "../Redux/Reducer";

const store = createStore(ToDoReducer);

store.dispatch({
    type: "SET_STATE",
    state: {
        items:[],
    }
});

const App = (props) => <Provider store={store}>
        <ToDo/>
    </Provider>;

export{
    App
}