import React from "react";

const Button = (props) => <button className="deleteButton" onClick={props.onClick}>{props.title}</button>;

export{
    Button
}