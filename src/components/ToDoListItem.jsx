import React from "react";
import { Button } from "./Button";

class ToDoListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            readOnly: true,
            inputValue: props.item.Name,
        }
    }
    keyPress = (e) => {
        if (e.key == "Enter") {
            this.props.onSave(this.props.item.Id, this.state.inputValue);
            this.setState({ readOnly: true });
        }
    }
    editClick = (e) => {
        this.setState({ readOnly: false });
    }
    inputChange =(e) =>{
        this.setState({ inputValue: event.target.value });
    }
    toDoneItem = () => {
        this.props.toDone(this.props.item.Id);
    }
    render() {
        const {status} = this.props.item;
        const {inputValue, readOnly} = this.state;

        return <li className={this.props.className}>
            <input className="checkBox"
                
                type={this.props.type}
                checked={status}
                onChange={this.toDoneItem}
            />
            {
                readOnly ?
                    <span className="text" onDoubleClick={this.editClick}>
                    {
                        inputValue
                    }
                    </span>
                    : <input minLength="2" maxLength="30" value={inputValue} onKeyPress={this.keyPress} onChange={this.inputChange}/>
            }
            <Button title="-" onClick={this.props.onDelete} />
        </li>;
    }
}

export {
    ToDoListItem
}